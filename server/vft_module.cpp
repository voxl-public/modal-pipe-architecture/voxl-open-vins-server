

#include "shared_vars.h"
#include "vft_module.h"

#include <stdio.h>
#include <vft_interface.h>
#include "common.h"
#include "cam_config_file.h"

char vft_name[CHAR_BUF_SIZE] = "tracked_feats";

vft_feature_packet* feature_packet_from_vft;
vft_feature* features_from_vft;

 std::deque<vft_feature_set> feature_queue;
std::mutex feature_queue_mtx;
vft_feature_set feat_set;
vft_feature_set feat_set_multi[4];
vft_feature_set feat_set_zero;
std::vector<vft_feature_set> feat_set_zero_multi;



static void _vft_disconnect_cb(__attribute__((unused)) int ch,
						__attribute__((unused)) void *context)
{
    destroy_vft_memory(&feature_packet_from_vft, &features_from_vft);
	return;
}

static void _vft_data_default_handler(__attribute__((unused)) int ch, char* data, int bytes,
                       __attribute__((unused)) void* context)
{

	// receving feature data, reset errors
	global_error_codes &= ~ERROR_CODE_CAM_MISSING;

	if (validate_vft_data(ch, data, bytes, &feature_packet_from_vft, &features_from_vft)) {
	    printf("ERROR parsing vft data from pipe...\n");
		return;
	}

	double set_time = (double) feature_packet_from_vft->timestamp_ns[0] * 1e-9;

	static double last_set_time = set_time;
	static int n_total_features = 0;

	double dt_f = set_time-last_set_time;
	last_set_time = set_time;

	if (dt_f == 0)
		return;

	if (is_resetting)
		return;

	// testing logic
	if (pause_cam_states[0] || pause_cam_states[1])
	{
		if (pause_cam_states[0])
		{
			feature_packet_from_vft->num_feats[0] = 0;
		}
		if (pause_cam_states[1])
		{
			feature_packet_from_vft->num_feats[1] = 0;
		}
	}


	if (!vio_manager->initialized() || imu_moved || en_vio_always_on)
	{
		n_total_features = 0;

		if (use_takeoff_cam && (is_armed || en_vio_always_on) && (double) alt_z < takeoff_threshold) // turn off, we are in the air
		{
			printf("Detected Takeoff, going to multicamera VINS normal operations\n");
			use_takeoff_cam = false;
		}

		int n_cams = feature_packet_from_vft->n_cams;

		for (int i = 0; i < n_cams; i++) {
			if (use_takeoff_cam && i == takeoff_cam)
			{
				n_total_features = 0;
				n_total_features = feature_packet_from_vft->num_feats[i];
				break;
			}
			else
			{
				n_total_features += feature_packet_from_vft->num_feats[i];
			}
		}

		std::vector<int> cams_used_for_takeoff;
		for (int i = 0; i < n_cams; i++) {
			auto it = std::find(takeoff_cams.begin(), takeoff_cams.end(), i);
			if (it != takeoff_cams.end()) {
				cams_used_for_takeoff.push_back(1);
			} else {
				cams_used_for_takeoff.push_back(0);
			}
		}

		

		if (use_takeoff_cam) {
			feat_set_zero_multi.resize(takeoff_cams.size());
		}


		feat_set_zero.features.resize(feature_packet_from_vft->num_feats[0]);
		std::map<double, int> ts_map;

		{
			int last_feat_ctn = 0;
			int takeoff_cam_ctn = 0;

			// break out features in to their own feat_set
			for (int z=0;z<cameras_used;z++)
			{
				double ts_cam =  (double) feature_packet_from_vft->timestamp_ns[z] * 1e-9;
			    auto it = ts_map.find(ts_cam);
			    if (it != ts_map.end())
			    {
			    	ts_map[ts_cam+0.0000001] = z;
			    }
			    else
			    {
			    	ts_map[ts_cam] = z;
			    }

				feat_set_multi[z].timestamp = ts_cam;
				feat_set_multi[z].cam_id = 0;

				int cam_total_features = feature_packet_from_vft->num_feats[z];
				feat_set_multi[z].features.resize(cam_total_features);
				
				if (cams_used_for_takeoff[z] > 0) {
					feat_set_zero_multi[takeoff_cam_ctn].features.resize(cam_total_features);
				}

				for (int x=0;x<cam_total_features;x++)
				{
					// set age threshold as deemed appropriate
					if (features_from_vft[last_feat_ctn].age >= 20) {
						feat_set_multi[z].features[x].cam_id = z;
						feat_set_multi[z].features[x].id = features_from_vft[last_feat_ctn].id;
						feat_set_multi[z].features[x].u = features_from_vft[last_feat_ctn].x;
						feat_set_multi[z].features[x].v = features_from_vft[last_feat_ctn].y;  // TODO subtract from height?
						memcpy(feat_set_multi[z].features[x].descriptor, features_from_vft[last_feat_ctn].descriptor, 32);

						if (cams_used_for_takeoff[z] > 0)
						{
							feat_set_zero_multi[takeoff_cam_ctn].cam_id = z;
							feat_set_zero_multi[takeoff_cam_ctn].features[x].cam_id = z;
							feat_set_zero_multi[takeoff_cam_ctn].features[x].id = features_from_vft[last_feat_ctn].id;
							feat_set_zero_multi[takeoff_cam_ctn].features[x].u = features_from_vft[last_feat_ctn].x;
							feat_set_zero_multi[takeoff_cam_ctn].features[x].v = features_from_vft[last_feat_ctn].y;  // TODO subtract from height?
							memcpy(feat_set_zero_multi[takeoff_cam_ctn].features[x].descriptor, features_from_vft[last_feat_ctn].descriptor, 32);
						}

						if (z == takeoff_cam)
						{
							feat_set_zero.cam_id = z;
							feat_set_zero.features[x].cam_id = z;
							feat_set_zero.features[x].id = features_from_vft[last_feat_ctn].id;
							feat_set_zero.features[x].u = features_from_vft[last_feat_ctn].x;
							feat_set_zero.features[x].v = features_from_vft[last_feat_ctn].y;  // TODO subtract from height?
							memcpy(feat_set_zero.features[x].descriptor, features_from_vft[last_feat_ctn].descriptor, 32);
						}
					}
					last_feat_ctn++;
				}

				if (cams_used_for_takeoff[z] > 0) {
					takeoff_cam_ctn++;
				}
			}

			int total_feats_ctn = 0;
			double avg_ts = 0;
			int num_cams_used = 0;
			feat_set.features.clear();
			for (const auto& pair : ts_map)
			{
				int cam_num = pair.second;

				if  (use_takeoff_cam && takeoff_cams.size() > 0  && cams_used_for_takeoff[cam_num] == 0)
				{
					continue;
				}

				num_cams_used++;
				avg_ts += feat_set_multi[cam_num].timestamp;
				feat_set.features.insert(feat_set.features.end(), feat_set_multi[cam_num].features.begin(), feat_set_multi[cam_num].features.end());

			}

			double cam_time =  avg_ts/num_cams_used;
//			double cam_time = ts_map.begin()->first;

			feat_set.timestamp = cam_time;
			feat_set_zero.timestamp = cam_time;
			std::lock_guard < std::mutex > lck(feature_queue_mtx);
			feature_queue.push_back(feat_set);
		}
	}
	else
	{
		double cam_time =  (double) feature_packet_from_vft->timestamp_ns[0] * 1e-9;
		// if idle for a long time, update the zero state by forcing a reset
		if (cam_time - feat_set_zero.timestamp > 30.0)
		{
			printf("Sitting around for a long time, resetting zero state for blind takeoff\n");
			init_failure_detector_reset_flag = 1;
		}

		feat_set.timestamp = cam_time;


		feat_set.features.clear();
		for (int i = 0; i < feat_set_zero_multi.size(); i++) {
			feat_set.features.insert(feat_set.features.end(), feat_set_zero_multi[i].features.begin(), feat_set_zero_multi[i].features.end());
		}

		std::lock_guard < std::mutex > lck(feature_queue_mtx);

		feature_queue.push_back(feat_set);
	}

	is_cam_connected = true;
}




int connect_vft_service(void)
{
	// connect to our feature tracker
	pipe_client_set_disconnect_cb(FEATURE_CH, _vft_disconnect_cb, NULL);
	pipe_client_set_simple_helper_cb(FEATURE_CH, _vft_data_default_handler, NULL);

	// allocate memory for vft data
	create_vft_memory(&feature_packet_from_vft, &features_from_vft);
	
	// open client pipe
	if (pipe_client_open(FEATURE_CH, vft_name, PROCESS_NAME, EN_PIPE_CLIENT_SIMPLE_HELPER /*CLIENT_FLAG_EN_SIMPLE_HELPER*/, 
			sizeof(vft_feature_packet)) != 0)
	{
		printf("failed to open vft client pipe\n");
		return -1;
	}

	printf("VFT connected\n");

	return 0;
}
